<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        h1 {
            color: salmon;
        }

    </style>
</head>

<body>
    {{-- <img src="http://127.0.0.1:8000/img/banner1.jpg" alt="" srcset=""> --}}
    <h1>Contacto vía https://fhtsrl.com/</h1>
    <p><strong>Nombre: </strong>{{ $contacto['name'] }}</p>
    <p><strong>Correo: </strong>{{ $contacto['email'] }}</p>
    <p><strong>Teléfono: </strong>{{ $contacto['phone'] }}</p>
    <p><strong>Mensaje: </strong>{{ $contacto['mensaje'] }}</p>
</body>

</html>
