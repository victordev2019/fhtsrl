<section id="contactanos" class="relative text-yellow-700">
    {{-- <h1 class="text-4xl p-4 font-bold tracking-wide">
        Información de contacto
    </h1> --}}
    {{-- <div class="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 bg-red-800 h-32 w-full"></div> --}}

    <!-- wrapper -->
    <div class="relative p-5 lg:px-20 flex flex-col md:flex-row items-center justify-center">
        <!-- Contact Me -->
        <form action="{{ route('contactanos.store') }}" method="POST"
            class="w-full md:w-1/2 border p-6 bg-gray-900 rounded-2xl">
            @csrf
            <h2 class="text-2xl pb-3 font-semibold text-blue-500 text-center uppercase">
                Enviar mensaje
            </h2>
            <div>
                <div class="flex flex-col mb-3">
                    <label for="name">Nombre</label>
                    <input type="text" id="name"
                        class="text-blue-400 px-3 py-2 bg-gray-800 border border-gray-900 focus:border-yellow-700 focus:outline-none focus:bg-gray-800 focus:text-blue-400"
                        autocomplete="off" name="name">
                    <br>
                    @error('name')
                        <p><strong class="text-red-700">{{ $message }}</strong></p>
                    @enderror
                </div>
                <div class="flex flex-col mb-3">
                    <label for="email">Email</label>
                    <input type="text" id="email"
                        class="text-blue-400 px-3 py-2 bg-gray-800 border border-gray-900 focus:border-yellow-700 focus:outline-none focus:bg-gray-800 focus:text-red-500"
                        autocomplete="off" name="email">
                    <br>
                    @error('email')
                        <p><strong class="text-red-700">{{ $message }}</strong></p>
                    @enderror
                </div>
                <div class="flex flex-col mb-3">
                    <label for="message">Mensaje</label>
                    <textarea rows="4" id="message"
                        class="text-blue-400 px-3 py-2 bg-gray-800 border border-gray-900 focus:border-yellow-700 focus:outline-none focus:bg-gray-800 focus:text-red-500"
                        name="mensaje"></textarea>
                    <br>
                    @error('mensaje')
                        <p><strong class="text-red-700">{{ $message }}</strong></p>
                    @enderror
                </div>
            </div>
            <div class="w-full pt-3">
                <button type="submit"
                    class="w-full bg-gray-900 border border-yellow-700 px-4 py-2 transition duration-50 focus:outline-none font-semibold hover:bg-yellow-700 hover:text-white text-xl cursor-pointer rounded-full">
                    Enviar
                </button>
            </div>
        </form>
    </div>
    @if (session('info'))
        <!-- component -->
        <div class="flex justify-center">
            <div class="flex bg-green-200 max-w-2xl mb-4">
                <div class="w-16 bg-green-700">
                    <div class="p-4">
                        <svg class="h-8 w-8 text-white fill-current" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 512 512">
                            <path
                                d="M468.907 214.604c-11.423 0-20.682 9.26-20.682 20.682v20.831c-.031 54.338-21.221 105.412-59.666 143.812-38.417 38.372-89.467 59.5-143.761 59.5h-.12C132.506 459.365 41.3 368.056 41.364 255.883c.031-54.337 21.221-105.411 59.667-143.813 38.417-38.372 89.468-59.5 143.761-59.5h.12c28.672.016 56.49 5.942 82.68 17.611 10.436 4.65 22.659-.041 27.309-10.474 4.648-10.433-.04-22.659-10.474-27.309-31.516-14.043-64.989-21.173-99.492-21.192h-.144c-65.329 0-126.767 25.428-172.993 71.6C25.536 129.014.038 190.473 0 255.861c-.037 65.386 25.389 126.874 71.599 173.136 46.21 46.262 107.668 71.76 173.055 71.798h.144c65.329 0 126.767-25.427 172.993-71.6 46.262-46.209 71.76-107.668 71.798-173.066v-20.842c0-11.423-9.259-20.683-20.682-20.683z" />
                            <path
                                d="M505.942 39.803c-8.077-8.076-21.172-8.076-29.249 0L244.794 271.701l-52.609-52.609c-8.076-8.077-21.172-8.077-29.248 0-8.077 8.077-8.077 21.172 0 29.249l67.234 67.234a20.616 20.616 0 0 0 14.625 6.058 20.618 20.618 0 0 0 14.625-6.058L505.942 69.052c8.077-8.077 8.077-21.172 0-29.249z" />
                        </svg>
                    </div>
                </div>
                <div class="w-auto text-green-700 items-center p-4">
                    <span class="text-lg font-bold pb-4">
                        Mensaje enviado correctamente.
                    </span>
                    <p class="leading-tight text-green-800">
                        Gracias por enviarnos los datos, nosotros nos pondremos en contacto para
                        responder a su mensaje.
                    </p>
                </div>
            </div>

        </div>
    @else
        {{-- <div class="flex bg-red-300 max-w-sm mb-4">
            <div class="w-16 bg-red">
                <div class="p-4">
                    <svg class="h-8 w-8 text-white fill-current" xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 512 512">
                        <path
                            d="M437.019 74.981C388.667 26.629 324.38 0 256 0S123.333 26.63 74.981 74.981 0 187.62 0 256s26.629 132.667 74.981 181.019C123.332 485.371 187.62 512 256 512s132.667-26.629 181.019-74.981C485.371 388.667 512 324.38 512 256s-26.629-132.668-74.981-181.019zM256 470.636C137.65 470.636 41.364 374.35 41.364 256S137.65 41.364 256 41.364 470.636 137.65 470.636 256 374.35 470.636 256 470.636z"
                            fill="#FFF" />
                        <path
                            d="M341.22 170.781c-8.077-8.077-21.172-8.077-29.249 0L170.78 311.971c-8.077 8.077-8.077 21.172 0 29.249 4.038 4.039 9.332 6.058 14.625 6.058s10.587-2.019 14.625-6.058l141.19-141.191c8.076-8.076 8.076-21.171 0-29.248z"
                            fill="#FFF" />
                        <path
                            d="M341.22 311.971l-141.191-141.19c-8.076-8.077-21.172-8.077-29.248 0-8.077 8.076-8.077 21.171 0 29.248l141.19 141.191a20.616 20.616 0 0 0 14.625 6.058 20.618 20.618 0 0 0 14.625-6.058c8.075-8.077 8.075-21.172-.001-29.249z"
                            fill="#FFF" />
                    </svg>
                </div>
            </div>
            <div class="w-auto text-black opacity-75 items-center p-4">
                <span class="text-lg font-bold pb-4">
                    Heads Up!
                </span>
                <p class="leading-tight">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam, nemo!
                </p>
            </div>
        </div> --}}
    @endif
</section>
