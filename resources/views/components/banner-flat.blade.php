<div class="banner-flat bg-{{$img}} z-0">
    <div class="container text-white relative tracking-wider">
        <div class="info absolute z-10 flex items-center">
            <div>
                <h1 class="uppercase font-bold py-2">{{$title}}</h1>
                <h2 class="uppercase py-1 font-normal">fahrenheit s.r.l.</h2>
                <p class="py-0 text-xl tracking-widest">{{$slot}}</p>
            </div>
        </div>
    </div>
</div>