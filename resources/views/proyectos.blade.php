@extends('layouts.main')
@section('title','Proyectos')
@section('description','Ingeniería especializada')
@section('content')
{{-- main banner --}}
<x-banner-flat img="banner-proyectos">
    <x-slot name="title">
        Proyectos
    </x-slot>
    Años de experiencia garantizan la calidad de nuestros servicios.
</x-banner-flat>
{{-- end main banner --}}
{{-- aside servicios --}}
<aside id="" class="galeria py-8">
   <div class="container">
    <h1 class="font-medium mb-8 uppercase">Destacados</h1>
    {{-- <h2></h2> --}}
    <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-5">
        <div class="item bg-gray-400">
            <img src="/img/proyectos/proyecto1.jpg" alt="" srcset="">
        </div>
        <div class="item bg-gray-400">
            <img src="/img/proyectos/proyecto2.jpg" alt="" srcset="">
        </div>
        <div class="item bg-gray-400">
            <img src="/img/proyectos/proyecto2.jpg" alt="" srcset="">
        </div>
        <div class="item bg-gray-400">
            <img src="/img/proyectos/proyecto4.jpg" alt="" srcset="">
        </div>
        <div class="item bg-gray-400">
            <img src="/img/proyectos/proyecto5.jpg" alt="" srcset="">
        </div>
        <div class="item bg-gray-400">
            <img src="/img/proyectos/proyecto6.jpg" alt="" srcset="">
        </div>
        <div class="item bg-gray-400">
            <img src="/img/proyectos/proyecto7.jpg" alt="" srcset="">
        </div>
        <div class="item bg-gray-400">
            <img src="/img/proyectos/proyecto8.jpg" alt="" srcset="">
        </div>
        <div class="item bg-gray-400">
            <img src="/img/proyectos/proyecto9.jpg" alt="" srcset="">
        </div>
        <div class="item bg-gray-400">
            <img src="/img/proyectos/proyecto10.jpg" alt="" srcset="">
        </div>
        <div class="item bg-gray-400">
            <img src="/img/proyectos/proyecto11.jpg" alt="" srcset="">
        </div>
        <div class="item bg-gray-400">
            <img src="/img/proyectos/proyecto12.jpg" alt="" srcset="">
        </div>
        <div class="item bg-gray-400">
            <img src="/img/proyectos/proyecto13.jpg" alt="" srcset="">
        </div>
        <div class="item bg-gray-400">
            <img src="/img/proyectos/proyecto14.jpg" alt="" srcset="">
        </div>
        <div class="item bg-gray-400">
            <img src="/img/proyectos/proyecto15.jpg" alt="" srcset="">
        </div>
        <div class="item bg-gray-400">
            <img src="/img/proyectos/proyecto16.jpg" alt="" srcset="">
        </div>
        <div class="item bg-gray-400">
            <img src="/img/proyectos/proyecto17.jpg" alt="" srcset="">
        </div>
        <div class="item bg-gray-400">
            <img src="/img/proyectos/proyecto18.jpg" alt="" srcset="">
        </div>
        <div class="item bg-gray-400">
            <img src="/img/proyectos/proyecto19.jpg" alt="" srcset="">
        </div>
    </div>
   </div>
</aside>
{{-- end aside servicios --}}
{{-- sección diseño estructural --}}

{{-- end diseño estructural --}}
{{-- sección últimos pryectos --}}

{{-- end últimos pryectos --}}
{{-- sección nuestros clientes --}}
{{-- end nuestros clientes --}}
@endsection