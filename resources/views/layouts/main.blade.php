<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="/favicon.ico">

    <title>{{ config('app.name', 'Laravel') }} | @yield('title')</title>

    <meta name="description" content=@yield("description") />

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" href="{{ mix('css/custom.css') }}">
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" /> --}}
    @livewireStyles
    <!-- Scripts -->
    {{-- <script src="https://www.google.com/recaptcha/api.js"></script> --}}
    <script src="{{ mix('js/app.js') }}" type="module" defer></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>

<body class="antialiased">
    @livewire('navigation')
    @yield('content')
    @livewire('mainfooter')

    @livewireScripts
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <script>
        console.log('prueba!!!');
        new WOW().init();
    </script>
    <script>
        Livewire.on('alert', function(titulo, messaje) {
            Swal.fire(
                titulo,
                messaje,
                'success'
            )
        });
    </script>
</body>

</html>
