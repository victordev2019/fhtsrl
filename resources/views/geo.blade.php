@extends('layouts.main')
@section('title','Geo5')
@section('description','Ingeniería especializada')
@section('content')
{{-- banner --}}
<div class="banner-geo">
    {{-- controles --}}
    <div id="prev" class="prev">
        <i class="fas fa-angle-left"></i>
    </div>
    <div id="next" class="next">
        <i class="fas fa-angle-right"></i>
    </div>
    {{-- slider --}}
    <div class="slider">
        <div class="slide slide-active" style="background-image: url('/img/geo/IMAGEN1.png');">
            <div class="container">
                <div class="content-slider flex">
                    <div class="caption self-center">
                        <img src="/img/logo_geo5_ bolivia.png" alt="">
                        <h1 class="font-semibold uppercase mb-6">interfaz fácil e intuitiva</h1>
                        <a href="https://wa.me/message/AWNRPEXYCKAID1" target="_blank" class="btng mr-4">comprar</a>
                        <a href="https://www.finesoftware.es/descarga/demo/" target="_blank" class="btng-outline">demo</a>
                    </div>
                </div>
                {{-- <div class="content-slider grid grid-cols-1 md:grid-cols-2">
                </div> --}}
            </div>
        </div>
        <div class="slide" style="background-image: url('/img/geo/IMAGEN2.png');">
            <div class="container">
                <div class="content-slider flex">
                    <div class="caption self-center">
                        <img src="/img/logo_geo5_ bolivia.png" alt="">
                        <h1 class="font-semibold uppercase mb-6">soporte a gran cantidad de normas!</h1>
                        <a href="https://wa.me/message/AWNRPEXYCKAID1" target="_blank" class="btng mr-4">comprar</a>
                        <a href="https://www.finesoftware.es/descarga/demo/" target="_blank" class="btng-outline">demo</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide" style="background-image: url('/img/geo/IMAGEN3.png');">
            <div class="container">
                <div class="content-slider flex">
                    <div class="caption self-center">
                        <img src="/img/logo_geo5_ bolivia.png" alt="">
                        <h1 class="font-semibold uppercase mb-6">del estudio geológico al diseño avanzado</h1>
                        <a href="https://wa.me/message/AWNRPEXYCKAID1" target="_blank" class="btng mr-4">comprar</a>
                        <a href="https://www.finesoftware.es/descarga/demo/" target="_blank" class="btng-outline">demo</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide" style="background-image: url('/img/geo/IMAGEN4.png');">
            <div class="container">
                <div class="content-slider flex">
                    <div class="caption self-center">
                        <img src="/img/logo_geo5_ bolivia.png" alt="">
                        <h1 class="font-semibold uppercase mb-6">programas vinculados</h1>
                        <a href="https://wa.me/message/AWNRPEXYCKAID1" target="_blank" class="btng mr-4">comprar</a>
                        <a href="https://www.finesoftware.es/descarga/demo/" target="_blank" class="btng-outline">demo</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide" style="background-image: url('/img/geo/IMAGEN5.png');">
            <div class="container">
                <div class="content-slider flex">
                    <div class="caption self-center">
                        <img src="/img/logo_geo5_ bolivia.png" alt="">
                        <h1 class="font-semibold uppercase mb-6">combinación de métodos analíticos y mef</h1>
                        <a href="https://wa.me/message/AWNRPEXYCKAID1" target="_blank" class="btng mr-4">comprar</a>
                        <a href="https://www.finesoftware.es/descarga/demo/" target="_blank" class="btng-outline">demo</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide" style="background-image: url('/img/geo/MUY PRONTO.png');">
            <div class="container">
                <div class="content-slider flex">
                    <div class="caption self-center">
                        <img src="/img/logo_geo5_ bolivia.png" alt="">
                        <h1 class="font-semibold uppercase mb-6">Muy pronto</h1>
                        <a href="https://wa.me/message/AWNRPEXYCKAID1" target="_blank" class="btng mr-4">comprar</a>
                        <a href="https://www.finesoftware.es/descarga/demo/" target="_blank" class="btng-outline">demo</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide" style="background-image: url('/img/geo/PAQUETES 5.png');">
            <div class="container">
                <div class="content-slider flex">
                    <div class="caption self-center">
                        <img src="/img/logo_geo5_ bolivia.png" alt="">
                        <h1 class="font-semibold uppercase mb-6">paquetes disponibles</h1>
                        <a href="https://wa.me/message/AWNRPEXYCKAID1" target="_blank" class="btng mr-4">comprar</a>
                        <a href="https://www.finesoftware.es/descarga/demo/" target="_blank" class="btng-outline">demo</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- indicadores --}}
    <div class="indicators">
        {{-- <div class="point">1</div>
        <div class="point">2</div>
        <div class="point">3</div> --}}
    </div>
</div>
{{-- end banner --}}

<section class="geo-paquetes py-8">
    <div class="container">
        <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
            <div class="pak-item">
                <div class="item-header">
                    <h4 class="uppercase">geología</h4>
                </div>
                <div class="item-body py-3">
                    <img src="/img/geo/1.png" class="mx-auto" alt="" width="150px">
                </div>
                <div class="item-text">
                    <p>Un paquete de programas
                        Geo5 esencial, que brindan
                        herramientas potentes para
                        hacer análisis de carácter
                        geológico estratigrafía,
                        perfiles geológicos.</p>
                    <h6 class="uppercase font-semibold underline py-3">programas</h6>
                    <p>4 programas de Geo5, comprendido Estratigrafía, Estratigrafía - Movimientos de Tierra, Estratigrafía - Perfiles Geológicos, Estratigrafía - Registros.</p>
                </div>
                <div class="item-footer mt-auto text-center">
                    <a href="https://wa.me/message/AWNRPEXYCKAID1" target="_blank" class="btng uppercase"><i class="fas fa-shopping-cart"></i> comprar</a>
                </div>
            </div>
            <div class="pak-item">
                <div class="item-header">
                    <h4 class="uppercase">Muro</h4>
                </div>
                <div class="item-body py-3">
                    <img src="/img/geo/2.png" class="mx-auto" alt="" width="150px">
                </div>
                <div class="item-text">
                    <p>Un paquete de programas
                        de Geo5 esencial, que
                        brinda soluciones
                        geotécnicas relacionadas
                        a estabilidad de taludes,
                        estratigrafía, fundaciones,
                        y muros de retención de
                        tierras prefabricadas.</p>
                    <h6 class="uppercase font-semibold underline py-3">programas</h6>
                    <p>7 programas de Geo5, comprendido Estabilidad de Taludes , Estratigrafía, Estratigrafía - Movimientos de Tierra, Muro Prefabricado, Muro Redi - Rock.</p>                        
                </div>
                <div class="item-footer mt-auto text-center">
                    <a href="https://wa.me/message/AWNRPEXYCKAID1" target="_blank" class="btng uppercase"><i class="fas fa-shopping-cart"></i> comprar</a>
                </div>
            </div>
            <div class="pak-item">
                <div class="item-header">
                    <h4 class="uppercase">Excavaciones</h4>
                </div>
                <div class="item-body py-3">
                    <img src="/img/geo/3.png" class="mx-auto" alt="" width="150px">
                </div>
                <div class="item-text">
                    <p>Un paquete de
                        programas de Geo5
                        esencial, que brinda la
                        posibilidad de analizar la
                        intervención en las
                        masas de suelo con
                        soluciones en muros
                        pantalla, pilotes
                        antideslizantes,
                        estabilidad de taludes y
                        empujes de tierra.</p>
                    <h6 class="uppercase font-semibold underline py-3">programas</h6>
                    <p>5 Programas de Geo5, comprendido Diseño de muros pantalla, estabilidad de Taludes, Pilote Anti-Deslizante Presiones de Tierra, verificación de Muros Pantalla.</p>
                </div>
                <div class="item-footer mt-auto text-center">
                    <a href="https://wa.me/message/AWNRPEXYCKAID1" target="_blank" class="btng uppercase"><i class="fas fa-shopping-cart"></i> comprar</a>
                </div>
            </div>
            <div class="pak-item">
                <div class="item-header">
                    <h4 class="uppercase">Cimentaciones</h4>
                </div>
                <div class="item-body py-3">
                    <img src="/img/geo/4.png" class="mx-auto" alt="" width="150px">
                </div>
                <div class="item-text">
                    <p>Un paquete de programas
                        de Geo5 esencial, con un
                        compendio geotécnico que
                        brinda la posibilidad de
                        analizar los diferentes
                        estructuras que se
                        presentan en la ingeniería
                        de cimentaciones.</p>
                    <h6 class="uppercase font-semibold underline py-3">programas</h6>
                    <p>8 programas de Geo5, comprendidas el Grupo de Pilotes, losa, Micropilote Pilote, Pilote por CPT, Viga, Zapata.</p>
                </div>
                <div class="item-footer mt-auto text-center">
                    <a href="https://wa.me/message/AWNRPEXYCKAID1" target="_blank" class="btng uppercase"><i class="fas fa-shopping-cart"></i> comprar</a>
                </div>
            </div>
            <div class="pak-item">
                <div class="item-header">
                    <h4 class="uppercase">básico</h4>
                </div>
                <div class="item-body py-3">
                    <img src="/img/geo/5.png" class="mx-auto" alt="" width="150px">
                </div>
                <div class="item-text">
                    <p>Un paquete de programas
                        de Geo5 esencial, con un
                        compendio geotécnico que
                        brinda la posibilidad de
                        analizar los diferentes
                        estructuras que se
                        presentan en la ingeniería
                        de cimentaciones.</p>
                    <h6 class="uppercase font-semibold underline py-3">programas</h6>
                    <p>28 programas Geo5, excepto los programas MEF, Asiento con Excavaciones y Excavación en pozo.</p>
                </div>
                <div class="item-footer mt-auto text-center">
                    <a href="https://wa.me/message/AWNRPEXYCKAID1" target="_blank" class="btng uppercase"><i class="fas fa-shopping-cart"></i> comprar</a>
                </div>
            </div>
            <div class="pak-item">
                <div class="item-header">
                    <h4 class="uppercase">profesional</h4>
                </div>
                <div class="item-body py-3">
                    <img src="/img/geo/6.png" class="mx-auto" alt="" width="150px">
                </div>
                <div class="item-text py-3">
                    <p>Un paquete de
                        programas de Geo5, que
                        brinda soluciones
                        específicas Geotécnicas
                        con desenvolvimiento
                        operativo eficiente y
                        eficaz en su empresa,
                        contando con todos los
                        programas geotécnicos
                        con módulos
                        especializados en MEF,
                        consolidaciones, sismos,
                        túneles y flujos de agua.</p>
                    <h6 class="uppercase font-semibold underline py-3">programas</h6>
                    <p>Contiene todos los programas de GEO5 con un total de 35 programas Geo5.</p>
                </div>
                <div class="item-footer mt-auto text-center">
                    <a href="https://wa.me/message/AWNRPEXYCKAID1" target="_blank" class="btng uppercase"><i class="fas fa-shopping-cart"></i> comprar</a>
                </div>
            </div>
        </div>
        <div class="pt-8">
            <a href="/docs/precios_package_geo5.pdf" target="_blank" class="font-semibold text-blue-500 hover:text-blue-400"><i class="far fa-file-pdf text-2xl"></i> Detalle Precios</a>
        </div>
    </div>
</section>

<section class="convenio pt-8 pb-12">
    <div class="container">
        <h1 class="uppercase py-2 font-medium">Convenio fahrenheit s.r.l. & geo5</h1>
        <div class="grid grid-cols-1 md:grid-cols-2 pt-8">
            <div class="convenio-desc p-3 card-light">
                <p>En marzo del año 2021 FAHRENHEIT SRL de Bolivia y FINE de República Checa, suscriben un convenio, dónde se convierten en distribuidores oficiales a nivel nacional de la marca GEO5, programa líder en soluciones geotecnicas con un gran abanico de opciones,  de esta forma FAHRENHEIT SRL Y FINE quieren aportar a la ingeniería nacional brindando el mejor producto en ingeniería geotecnica para sus usuarios.</p>
            </div>
            <div class="text-center p-3"><i class="fas fa-handshake text-9xl"></i></div>
        </div>
    </div>
</section>

{{-- <section class="g_sociales">
    <div class="container">
        <h1 class="uppercase">redes sociales</h1>
    </div>
</section> --}}
{{-- <script src="{{ asset('js/slidergeo.js') }}" defer></script> --}}

@endsection