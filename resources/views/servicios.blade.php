@extends('layouts.main')
@section('title','Servicios')
@section('description','Ingeniería especializada')
@section('content')
{{-- main banner --}}
<x-banner-flat img="banner-servicios">
    <x-slot name="title">
        Servicios
    </x-slot>
    Todo nuestro conocimiento y experiencia a su disposición.
</x-banner-flat>
{{-- end main banner --}}
{{-- sección construcción --}}
<section id="tag_construccion" class="construccion py-8 mt-8">
   <div class="container">
	   <h1 class="uppercase font-medium py-2">construcción</h1>
	   <h2 class="uppercase py-2">obras que perduran…</h2>
	   <div class="grid grid-cols-1 sm:grid-cols-2 gap-4 py-4">
		   <div class="construccion-item card-light p-4">
				<h3>Construcción en:</h3>
				  <ol>
					  <li><i class="far fa-check-circle text-2xl"></i> Edificios</li>
					  <li><i class="far fa-check-circle text-2xl"></i> Locales</li>
					  <li><i class="far fa-check-circle text-2xl"></i> Viviendas</li>
					  <li><i class="far fa-check-circle text-2xl"></i> Colegios</li>
					  <li><i class="far fa-check-circle text-2xl"></i> Almacenes</li>
					  <li><i class="far fa-check-circle text-2xl"></i> Galpones</li>
					  <li><i class="far fa-check-circle text-2xl"></i> Tinglados</li>
					  <li><i class="far fa-check-circle text-2xl"></i> Coliseos</li>
					  <li><i class="far fa-check-circle text-2xl"></i> Sistemas de riego</li>
					  <li><i class="far fa-check-circle text-2xl"></i> Sistemas de drenaje</li>
					  <li><i class="far fa-check-circle text-2xl"></i> Muros de contención</li>
					  <li><i class="far fa-check-circle text-2xl"></i> Mejoramiento vial</li>
					  <li><i class="far fa-check-circle text-2xl"></i> Obras de saneamiento</li>
					  <li><i class="far fa-check-circle text-2xl"></i> Obras industriales</li>
					  <li><i class="far fa-check-circle text-2xl"></i> Centros de salud</li>
					  <li><i class="far fa-check-circle text-2xl"></i> Refuerzos estructurales</li>
					  <li><i class="far fa-check-circle text-2xl"></i> Supervisión de obras</li>
				  </ol> 
				</div>
				<div class="construccion-item-img flex items-center relative">
					<img src="/img/banner1.jpg" class="rounded-lg" alt="" srcset="">
				</div>
			</div>
			<a href="https://wa.me/message/NLF65UVKBUBJA1" target="_blank" class="uppercase btn mr-4 mt-4">cotización</a>
			<a href="https://wa.me/message/AWNRPEXYCKAID1" target="_blank" class="uppercase btn-outline mt-4">contactos</a>
   </div>
</section>
{{-- end construcción --}}
<section id="tag_consultoria" class="consultoria py-8 mt-8">
    <div class="container">
		<div class="flex flex-col items-end">
			<h1 class="uppercase font-medium py-2">consultoría especializada</h1>
			<h2 class="flex flex-wrap uppercase py-2">Tipo de consultoría</h2>
		</div>
	    <div class="grid grid-cols-1 sm:grid-cols-2 gap-4 py-4">
			<div class="consultoria-item-img flex items-center relative">
				<img src="/img/banner2.jpg" class="rounded-lg" alt="" srcset="">
			</div>
			<div class="consultoria-item card-light p-4">
				<h3>Consultorías en:</h3>
				<ul>
					<li><i class="far fa-check-circle text-2xl"></i> Ingeniería estructural</li>
					<li><i class="far fa-check-circle text-2xl"></i> Ingeniería geotécnica</li>
					<li><i class="far fa-check-circle text-2xl"></i> Ingeniería hidráulica</li>
					<li><i class="far fa-check-circle text-2xl"></i> Ingeniería ambiental</li>
					<li><i class="far fa-check-circle text-2xl"></i> Ingeniería de vías y pavimentos</li>
					<li><i class="far fa-check-circle text-2xl"></i> Seguridad industrial</li>
					<li><i class="far fa-check-circle text-2xl"></i> Proyectos de pre inversión en general</li>
				</ul> 
			</div>
		</div>
		<div class="flex justify-end">
			<a href="https://wa.me/message/NLF65UVKBUBJA1" target="_blank" class="uppercase btn_secondary mr-4 mt-4">cotización</a>
			<a href="https://wa.me/message/AWNRPEXYCKAID1" target="_blank" class="uppercase btn-outline_secondary mt-4">contactos</a>
		</div>
    </div>
</section>
{{-- sección diseño estructural --}}
{{-- servicio arquitectura --}}
<section id="tag_arquitectura" class="arquitectura py-8 mt-8">
    <div class="container">
		<div class="flex flex-col items-start">
			<h1 class="uppercase font-medium py-2">servicio de arquitectura</h1>
			<h2 class="flex flex-wrap uppercase py-2">Tipo de servicio</h2>
		</div>
		<div class="grid grid-cols-1 sm:grid-cols-2 gap-4 py-4">
			<div class="arquitectura-item card-light p-4">
				<h3>Arquitectura en:</h3>
				<ul>
					<li><i class="far fa-check-circle text-2xl"></i> Concepción arquitectónica</li>
					<li><i class="far fa-check-circle text-2xl"></i> Modelado 3d</li>
					<li><i class="far fa-check-circle text-2xl"></i> Diseño arquitectónico (planos)</li>
					<li><i class="far fa-check-circle text-2xl"></i> Remodelaciones</li>
					<li><i class="far fa-check-circle text-2xl"></i> Diseño de interiores</li>
				</ul> 
			</div>
			<div class="arquitectura-item-img flex items-center relative">
				<img src="/img/banner4.jpg" class="rounded-md" alt="" srcset="">
			</div>
		</div>
		<div class="flex justify-start">
			<a href="https://wa.me/message/NLF65UVKBUBJA1" target="_blank" class="uppercase btn mr-4 mt-4">cotización</a>
			<a href="https://wa.me/message/AWNRPEXYCKAID1" target="_blank" class="uppercase btn-outline mt-4">contactos</a>
		</div>
    </div>
</section>
{{-- end arquitectura --}}
{{-- servicio arquitectura --}}
<section id="tag_diseno" class="servicio-diseno py-8 mt-8">
    <div class="container">
		<div class="flex flex-col items-end">
			<h1 class="uppercase font-medium py-2">ÁREA DE CÁLCULO Y DISEÑO</h1>
			<h2 class="flex flex-wrap uppercase py-2">Tipos de cálculo & diseño</h2>
		</div>
		<div class="grid grid-cols-1 sm:grid-cols-2 gap-4 py-4">
			<div class="servicio-diseno-item-img flex items-center relative">
				<img src="/img/banner3.jpg" class="rounded-md" alt="" srcset="">
			</div>
			<div class="servicio-diseno-item card-light p-4">
				<h3>Cálculo & Diseño en:</h3>
				<ul>
					<li><i class="far fa-check-circle text-2xl"></i> Edificios de hormigón armado</li>
					<li><i class="far fa-check-circle text-2xl"></i> Edificios metálicos</li>
					<li><i class="far fa-check-circle text-2xl"></i> Puentes y viaductos</li>
					<li><i class="far fa-check-circle text-2xl"></i> Galpones y tinglados</li>
					<li><i class="far fa-check-circle text-2xl"></i> Subestaciones eléctricas</li>
					<li><i class="far fa-check-circle text-2xl"></i> Plantas industriales</li>
					<li><i class="far fa-check-circle text-2xl"></i> Refuerzos estructurales</li>
					<li><i class="far fa-check-circle text-2xl"></i> Naves industriales</li>
					<li><i class="far fa-check-circle text-2xl"></i> Estabilidad de taludes y muros de contención</li>
					<li><i class="far fa-check-circle text-2xl"></i> Instalaciones sanitarias en edificaciones</li>
					<li><i class="far fa-check-circle text-2xl"></i> Proyectos de riego</li>
					<li><i class="far fa-check-circle text-2xl"></i> Vías de acceso y carreteras</li>
					<li><i class="far fa-check-circle text-2xl"></i> Edificaciones sismo resistentes</li>
					<li><i class="far fa-check-circle text-2xl"></i> Fundaciones superficiales</li>
					<li><i class="far fa-check-circle text-2xl"></i> Torres eléctricas y de telecomunicación</li>
					<li><i class="far fa-check-circle text-2xl"></i> Pavimentos rígidos, flexibles y articulados</li>
					<li><i class="far fa-check-circle text-2xl"></i> Presas y embalses</li>
				</ul> 
			</div>
		</div>
        <div class="flex justify-end">
			<a href="https://wa.me/message/NLF65UVKBUBJA1" target="_blank" class="uppercase btn_secondary mr-4 mt-4">cotización</a>
			<a href="https://wa.me/message/AWNRPEXYCKAID1" target="_blank" class="uppercase btn-outline_secondary mt-4">contactos</a>
		</div>
    </div>
</section>
{{-- end sección arquitectura --}}


{{-- end diseño estructural --}}
<section id="tag_geo" class="geo py-8 mt-8">
    <div class="container">
		<div class="flex flex-col items-start">
			<h1 class="uppercase font-medium py-2">GEO5 Soporte.</h1>
			<h2 class="flex flex-wrap uppercase py-2">Servicio de Soporte ágil.</h2>
			<h3>Para lograr una excelente atención al cliente</h3>
		</div>
		
		<div class="flex justify-center py-4">
			<a href="{{route('geo5')}}" class="hover:opacity-50"><img src="{{asset('/img/logo_geo5_ bolivia.png')}}" alt="" srcset="" width="80px"></a>
		</div>
		<div class="grid grid-cols-1 md:grid-cols-2 gap-4">
			<div class="item-geo card-light p-4">
				<h2>Muros.</h2>
				<p>Dentro la ingeniería geotécnica y estructural uno de los problemas con bastante recurrencia es la contención de tierras, esto ya sea para construcción de viviendas, apertura de vías u obras civiles que necesiten este tipo de estructuras por tal razón las soluciones deben ser optimas, seguras e inmediatas, para este tipo de problemas GEO5 es la mejor alternativa para conseguir estos fines.</p>
			</div>
			<div class="item-geo card-light p-4">
				<ul>
					<li><i class="far fa-check-circle text-2xl"></i> Estabilidad de Taludes.</li>
					<li><i class="far fa-check-circle text-2xl"></i> Suelo Reforzado.</li>
					<li><i class="far fa-check-circle text-2xl"></i> Voladizo.</li>
					<li><i class="far fa-check-circle text-2xl"></i> Gravedad.</li>
					<li><i class="far fa-check-circle text-2xl"></i> Gaviones.</li>
				</ul>
			</div>
			<div class="item-geo card-light p-4">
				<h2>Muros de Pantalla.</h2>
				<p>Cuando se efectúan excavaciones profundas o sótanos de consideración una de las opciones que se usa con recurrencia es la ejecución de muros pantalla, los cuales tiene el fin de contención de tierra, para ello estas estructuras deben tener un adecuado diseño para brindar en la ejecución seguridad y eficiencia en el tiempo de ejecución, GEO5 entre sus múltiples módulos brinda la opción de poder conseguir un diseño seguro, eficiente y óptimo para la proyección de este tipo de estructuras.</p>
			</div>
			<div class="item-geo card-light p-4">
				<ul>
					<li><i class="far fa-check-circle text-2xl"></i> Verificación.</li>
					<li><i class="far fa-check-circle text-2xl"></i> Diseño</li>
				</ul>
			</div>
			<div class="item-geo card-light p-4">
				<h2>Fundaciones Superficiales.</h2>
				<p>Las fundaciones superficiales son las encargadas de trasmitir toda la carga de la superestructura al suelo, ya sea un edificio, una nave industrial, obras de contención, etc. Por tal motivo es esencial contar con un diseño adecuado que brinde los mejores estándares de calidad, seguridad y optimización de recursos, por esta razón GEO5 al contar con múltiples módulos con el fin de evaluar y diseñar fundaciones superficiales, cumple de sobre manera las exigencias técnicas que necesitan estas estructuras.</p>
			</div>
			<div class="item-geo card-light p-4">
				<ul>
					<li><i class="far fa-check-circle text-2xl"></i> Asentamiento.</li>
					<li><i class="far fa-check-circle text-2xl"></i> Zapatas.</li>
					<li><i class="far fa-check-circle text-2xl"></i> Loza.</li>
				</ul>
			</div>
			<div class="item-geo card-light p-4">
				<h2>Fundaciones Profundas.</h2>
				<p>Cuando se presenta suelos con estratos de cuelo de baja resistencia se usan fundaciones profundas como ser pilotes, para alcanzar estratos de una resistencia adecuada y poder trasmitir las cargas de las edificaciones al suelo, por esta razón debemos contar con un diseño adecuado de fundaciones profundas para que no existan problemas de asentamientos indeseados y pongan en riesgo la estructura que estas fundaciones sostienen, GEO5 tiene una gama de módulos que ofrece la mejor opción para el diseño de estas estructuras especiales.</p>
			</div>
			<div class="item-geo card-light p-4">
				<ul>
					<li><i class="far fa-check-circle text-2xl"></i> Pilotes.</li>
					<li><i class="far fa-check-circle text-2xl"></i> Micropilotes.</li>
				</ul>
			</div>
		</div>
		<div class="flex justify-center py-4">
			<a href="{{route('geo5')}}" class="hover:opacity-50"><img src="{{asset('/img/logo_geo5_ bolivia.png')}}" alt="" srcset="" width="80px"></a>
		</div>
    </div>
 </section>
{{-- sección últimos pryectos --}}
<section id="tag_capacitacion" class="capacitacion py-8 mt-8">
    <div class="container">
		<h1 class="uppercase font-medium py-2">CAPACITACIONES</h1>
		<p class="text-2xl py-2">Guiamos, asesoramos y acompañamos en Tesis y Proyectos de Ingeniería Civil</p>
		<h2 class="flex flex-wrap uppercase justify-center py-2">CURSOS/CAPACITACIÓN</h2>
		<div class="tbl-capacitacion">
			{{-- tabla --}}
			<!-- This example requires Tailwind CSS v2.0+ -->
			<div class="flex flex-col">
				<div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
				<div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
					<div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
					<table class="min-w-full divide-y divide-gray-200">
						<thead class="bg-gray-50">
						<tr>
							<th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
							Nombre
							</th>
							<th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
							descripción
							</th>
							<th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
							programas
							</th>
							<th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
							duración
							</th>
							<th scope="col" class="relative px-6 py-3">
							<span class="sr-only">Edit</span>
							</th>
						</tr>
						</thead>
						<tbody class="bg-white divide-y divide-gray-200">
						<tr>
							<td class="px-6 py-4 whitespace-nowrap">
							<div class="flex items-center">
								<div class="flex-shrink-0 h-10 w-10">
								<img class="h-10 w-10 rounded-full" src="{{asset('/img/fht_logo_alt.svg')}}" alt="">
								</div>
								<div class="ml-4">
								<div class="text-sm font-medium text-gray-900">
									Ninguno
								</div>
								<div class="text-sm text-gray-500">
									Ninguno
								</div>
								</div>
							</div>
							</td>
							<td class="px-6 py-4 whitespace-nowrap">
							<div class="text-sm text-gray-900">Ninguno</div>
							<div class="text-sm text-gray-500">Ninguno</div>
							</td>
							<td class="px-6 py-4 whitespace-nowrap">
							<span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
								Ninguno
							</span>
							</td>
							<td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
							Admin
							</td>
							<td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
							<a href="#" class="text-indigo-600 hover:text-indigo-900">Edit</a>
							</td>
						</tr>
						<tr>
							<td class="px-6 py-4 whitespace-nowrap">
							<div class="flex items-center">
								<div class="flex-shrink-0 h-10 w-10">
								<img class="h-10 w-10 rounded-full" src="{{asset('/img/fht_logo_alt.svg')}}" alt="">
								</div>
								<div class="ml-4">
								<div class="text-sm font-medium text-gray-900">
									Ninguno
								</div>
								<div class="text-sm text-gray-500">
									Ninguno
								</div>
								</div>
							</div>
							</td>
							<td class="px-6 py-4 whitespace-nowrap">
							<div class="text-sm text-gray-900">Ninguno</div>
							<div class="text-sm text-gray-500">Ninguno</div>
							</td>
							<td class="px-6 py-4 whitespace-nowrap">
							<span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
								Ninguno
							</span>
							</td>
							<td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
							Admin
							</td>
							<td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
							<a href="#" class="text-indigo-600 hover:text-indigo-900">Edit</a>
							</td>
						</tr>
						<tr>
							<td class="px-6 py-4 whitespace-nowrap">
							<div class="flex items-center">
								<div class="flex-shrink-0 h-10 w-10">
								<img class="h-10 w-10 rounded-full" src="{{asset('/img/fht_logo_alt.svg')}}" alt="">
								</div>
								<div class="ml-4">
								<div class="text-sm font-medium text-gray-900">
									Ninguno
								</div>
								<div class="text-sm text-gray-500">
									Ninguno
								</div>
								</div>
							</div>
							</td>
							<td class="px-6 py-4 whitespace-nowrap">
							<div class="text-sm text-gray-900">Ninguno</div>
							<div class="text-sm text-gray-500">Ninguno</div>
							</td>
							<td class="px-6 py-4 whitespace-nowrap">
							<span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
								Ninguno
							</span>
							</td>
							<td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
							Admin
							</td>
							<td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
							<a href="#" class="text-indigo-600 hover:text-indigo-900">Edit</a>
							</td>
						</tr>
						<!-- More items... -->
						</tbody>
					</table>
					</div>
				</div>
				</div>
			</div>
			{{-- end tabla --}}
		</div>
    </div>           
 </section>
{{-- end últimos pryectos --}}
{{-- sección asesoría académica --}}
<section id="tag_asesoria" class="asesoria py-8 mt-8">
    <div class="container">
		<h1 class="uppercase font-medium py-2">asesoría académica</h1>
		<h2 class="flex flex-wrap uppercase py-2">Servicio de Soporte ágil.</h2>
	    <h3 class="py-2">Para lograr una excelente atención al cliente</h3>
	    {{-- <i class="fas fa-file-signature text-3xl"></i> --}}
        <div class="modalidad">
			<h3>Modalidad 1. Tesis.</h3>
			<ol>
				<li>Estabilidad de Taludes.</li>
				<li>Suelo Reforzado.</li>
				<li>Voladizo.</li>
				<li>Gravedad.</li>
				<li>Gaviones.</li>
			</ol>
			<h3>Modalidad 2. Proyectos de Grado.</h3>
			<ol>
				<li>Proyectos de Grado.</li>
				<li>Trabajo Dirigido.</li>
			</ol>
	   </div>
	   <hr>
	   <div class="planes">
		   <h3>Plan I.</h3>
		   <h4>Contenido.</h4>
		   <h4>INGENIERIA DEL PROYECTO.</h4>
		   <ul>
			   <li>Memoria de cálculo.</li>
			   <li>Aspectos Normativos.</li>
			   <li>Diseño y cálculos detallados.</li>
			   <li>Análisis de precios unitarios.</li>
			   <li>Análisis Financiero.</li>
			   <li>Evaluación Financiera.</li>
			   <li>Planos constructivos.</li>
			   <li>Propuesta Organizacional.</li>
			   <li>Viabilidad.</li>
			   <li>Herramientas audiovisuales.</li>
			   <li>Bibliografía especializada.</li>
			   <li>Tutoriales Personalizados.</li>
		   </ul>
		   <h3>Plan II.</h3>
		   <h4>Contenido.</h4>
		   <h4>INGENIERIA DEL PROYECTO + FUNDAMENTOS TEORICOS.</h4>
		   <h4>FUNDAMENTOS TEORICOS.</h4>
		   <ul>
			   <li>Objetivo General.</li>
			   <li>Objetivo Específico.</li>
			   <li>Estado del Arte.</li>
			   <li>Marco Conceptual.</li>
			   <li>Marco Teórico.</li>
			   <li>Marco Metodológico.</li>
			   <li>Marco Legal.</li>
			   <li>Marco Normativo.</li>
			   <li>Marco Aplicativo.</li>
		   </ul>
		   <h4>INGENIERIA DEL PROYECTO.</h4>
		   <ul>
			   <li>Memoria de cálculo.</li>
			   <li>Aspectos Normativos.</li>
			   <li>Diseño y cálculos detallados.</li>
			   <li>Análisis de precios unitarios.</li>
			   <li>Análisis Financiero.</li>
			   <li>Evaluación Financiera.</li>
			   <li>Planos constructivos.</li>
			   <li>Propuesta Organizacional.</li>
			   <li>Viabilidad.</li>
			   <li>Herramientas audiovisuales.</li>
			   <li>Bibliografía especializada.</li>
			   <li>Tutoriales Personalizados.</li>
		   </ul>
		   <h3>Plan II.</h3>
		   <h4>Contenido.</h4>
		   <h4>PERFIL + INGENIERIA DEL PROYECTO + FUNDAMENTOS TEORICOS.</h4>
		   <h4>PERFIL</h4>
		   <ul>
			   <li>Tema de Proyecto de Grado.</li>
			   <li>Antecedentes</li>
			   <li>Justificación.</li>
			   <li>Planteamiento del Problema.</li>
			   <li>Marco Conceptual y Teórico.</li>
			   <li>Marco Legal y Normativo.</li>
			   <li>Cadena de Valor.</li>
			   <li>Marco Institucional o Involucrados.</li>
			   <li>Objetivos.</li>
			   <li>Estrategia Metodológica.</li>
			   <li>Objetivos.</li>
			   <li>Índice y Contenido tentativo.</li>
			   <li>Naturaleza del Proyecto.</li>
			   <li>Alcance.</li>
			   <li>Cronograma.</li>
			   <li>Presupuesto de Proyecto.</li>
		   </ul>
		   <h4>FUNDAMENTOS TEORICOS.</h4>
		   <ul>
			   <li>Objetivo General.</li>
			   <li>Objetivo Específico.</li>
			   <li>Estado del Arte.</li>
			   <li>Marco Conceptual.</li>
			   <li>Marco Teórico.</li>
			   <li>Marco Metodológico.</li>
			   <li>Marco Legal.</li>
			   <li>Marco Normativo.</li>
			   <li>Marco Aplicativo.</li>
		   </ul>
		   <h4>INGENIERIA DEL PROYECTO.</h4>
		   <ul>
			   <li>Memoria de cálculo.</li>
			   <li>Aspectos Normativos.</li>
			   <li>Diseño y cálculos detallados.</li>
			   <li>Análisis de precios unitarios.</li>
			   <li>Análisis Financiero.</li>
			   <li>Evaluación Financiera.</li>
			   <li>Planos constructivos.</li>
			   <li>Propuesta Organizacional.</li>
			   <li>Viabilidad.</li>
			   <li>Herramientas audiovisuales.</li>
			   <li>Bibliografía especializada.</li>
			   <li>Tutoriales Personalizados.</li>
		   </ul>
	   </div>
    </div>
 </section>
{{-- end asesoría académica --}}
@endsection