@extends('layouts.main')
@section('title','Acerca de')
@section('description','Ingeniería especializada')
@section('content')
{{-- banner --}}
<x-banner-flat img="hero-pattern">
   <x-slot name="title">
       acerca de
   </x-slot>
   Ingeniería especializada.
</x-banner-flat>
{{-- end banner --}}
{{-- aside servicios --}}
<section id="" class="acercade py-8">
   <div class="container">
      <div class="pt-4 pb-8">
         <h2 class="pb-4 uppercase">Sobre Fahrenheit S.R.L.</h2>
         <p>Nosotros somos Fahrenheit, una empresa especializada en Ingeniería civil y arquitectura que participa activamente en la industria de la construcción, consultorías, capacitación para profesionales y formación académica.</p> 
         <br>
         <p>Nuestra trayectoria y crecimiento se encuentra en desarrollo desde el 2012, brindando a nuestros clientes resultados de puntualidad, responsabilidad, seriedad y eficiencia con más de 8 años de experiencia en el mercado.</p>
      </div>
      <div class="preceptos grid grid-cols-1 md:grid-cols-2 items-center py-4 gap-4">
         <div class="order-1 md:order-2">
            <h3 class="pb-2 uppercase">Nuestra misión</h3>
            <p>Desarrollar servicios de calidad de manera integral en ingeniería civil y arquitectura, demostrando el profesionalismo en el rubro de la construcción en proyectos de vanguardia, consultoría, asesoramiento técnico especializado y capacitación para profesionales. Contando con el mejor equipo de profesionales nacionales e internacionales.</p>
         </div>
         <div class="order-2 md:order-1">
            <img src="/img/mission.png" alt="Mission imagen" srcset="">
         </div>
         <div class="order-3">
            <h3 class="pb-2 uppercase">Nuestra visión</h3>
            <p>Posicionarse como empresa líder a nivel nacional e internacional en el rubro de ingeniería civil y arquitectura, reconocida por su calidad de servicio y su equipo especializado.</p>
         </div>
         <div class="order-4">
            <img src="/img/vision.png" alt="Vision imagen" srcset="">
         </div>
         <div class="order-5 md:order-6">
            <h3 class="pb-2 uppercase">Valores/Objetivos</h3>
            <p class="mb-2"><span class="font-semibold underline">Calidad:</span> innovación constante en los procesos, entrega de productos y servicios diferenciadores, con valor agregado a cada uno de nuestros clientes.</p>
            <p class="mb-2"><span class="font-semibold underline">Solidaridad:</span>  Anteponer el bienestar colectivo al lucro, tendiendo una mano al cliente externo e interno de Fahrenheit S.R.L.</p>
            <p class="mb-2"><span class="font-semibold underline">Responsabilidad:</span> consciencia de la importancia del trabajo en el tiempo, con las condiciones establecidas para nuestros clientes.</p>
            <p class="mb-2"><span class="font-semibold underline">Confianza:</span> generar relaciones estables y productivas a través de la trasparencia y honestidad con nuestros socios y clientes.</p>
            <p class="mb-2"><span class="font-semibold underline">Trabajo en equipo:</span> orgullosos de pertenecer a esta organización inspiramos lo mejor para el crecimiento constante de la empresa y las comunidades impactadas enfocados hacia un mismo objetivo.</p>
            <p class="mb-2"><span class="font-semibold underline">Sinceridad:</span> compromiso con el ejercicio de la decencia, el decoro, la prudencia y la justicia para con nosotros mismos y con los demás.</p>
            <p class="mb-2"><span class="font-semibold underline">Originalidad:</span> Enfocados en la calidad y mejora continua de los productos y servicios que ofrece Fahrenheit S.R.L.</p>
            <p><span class="font-semibold underline">Competitividad:</span> esfuerzo por ser los primeros en el sector, planteando objetivos que son destino donde llegar con eficacia y eficiencia.</p>
         </div>
         <div class="order-6 md:order-5">
            <img src="/img/values.png" alt="Valores imagen" srcset="" width="600">
         </div>
      </div>
   </div>
</section>
{{-- end aside servicios --}}
{{-- sección nuestro equipo --}}
<section class="equipo py-8">
   <div class="container">
      <h2 class="pb-3 uppercase">Nuestro equipo</h2>
      <p>Conoce a nuestro equipo de trabajo.</p>
      <div class="equipo-hero flex flex-wrap justify-center py-8">
         <img src="/img/employes/employe1.jpg" alt="" srcset="">
         <img src="/img/employes/employe2.jpg" alt="" srcset="">
      </div>
   </div>
</section>
{{-- end nuestro equipo --}}
{{-- sección últimos pryectos --}}
{{-- end últimos pryectos --}}
{{-- sección nuestros clientes --}}
{{-- end nuestros clientes --}}
@endsection