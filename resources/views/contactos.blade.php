@extends('layouts.main')
@section('title', 'Contactos')
@section('description', 'Ingeniería especializada')
@section('content')
    {{-- main banner --}}
    <x-banner-flat img="banner-direcction">
        <x-slot name="title">
            contactos
        </x-slot>
        Búscanos en todas nuestras direcciones.
    </x-banner-flat>
    {{-- end main banner --}}
    {{-- aside servicios --}}
    {{-- LA PAZ --}}
    <aside id="" class="aside-direccion py-2 mt-8">
        <div class="container">
            <div class="grid md:grid-cols-2 gap-4">
                <div class="dir-left rounded-lg">
                    <h3 class="font-medium">La Paz - Bolivia <span class="text-lg font-normal">OFICINA CENTRAL</span></h3>
                    <p class="py-2"><i class="fas fa-map-marker-alt"></i> Edif. Alianza - Av. 6 de Agosto Esquina
                        Guachalla
                        - Piso 1 Of. 107 / 108</p>
                    <p class="py-2"><i class="fas fa-phone-alt"></i> +591 76725400 - +591 73725190</p>
                    <p><i class="fas fa-envelope"></i> ingenieria.fht@gmail.com</p>
                </div>
                <div class="dir-right rounded-lg">
                    <img class="rounded-lg" src="/img/direccion.png" alt="" srcset="">
                </div>
            </div>
        </div>
    </aside>
    {{-- SANTA CRUZ --}}
    <aside id="" class="aside-direccion py-2">
        <div class="container">
            <div class="grid md:grid-cols-2 gap-4">
                <div class="dir-left rounded-lg">
                    <h3 class="font-medium">Santa Cruz - Bolivia <span class="text-lg font-normal">OFICINA CENTRAL</span>
                    </h3>
                    <p class="py-2"><i class="fas fa-map-marker-alt"></i> Equipetrol Norte. Edificio Rolea Center
                        N 120
                        Primer Piso Oficina 1A
                    </p>
                    <p class="py-2"><i class="fas fa-phone-alt"></i> +591 70226327 - +591 67865550</p>
                    <p><i class="fas fa-envelope"></i> ingenieria.fht@gmail.com</p>
                </div>
                <div class="dir-right rounded-lg">
                    <img class="rounded-lg" src="/img/Equipetrol.png" alt="" srcset="">
                </div>
            </div>
        </div>
    </aside>

    {{-- end aside servicios --}}
    {{-- sección diseño estructural --}}
    <section class="map mt-2 mb-4 z-0">
        <div class="container relative z-10">
            <div class="grid grid-cols-1 sm:grid-cols-2 gap-4">
                <div>
                    <h3 class="text-center mb-2">La Paz</h3>
                    <iframe class="mapa-google rounded-lg"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d568.645176207477!2d-68.12764054488603!3d-16.507230010725117!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x915f20647f5fdc11%3A0xea272ad610a713dd!2sEdificio%20Alianza!5e0!3m2!1ses!2sbo!4v1617593056511!5m2!1ses!2sbo"
                        width="100%" height="450" style="border:0;" allowfullscreen="true" loading="lazy"></iframe>

                </div>
                <div>
                    <h3 class="text-center mb-2">Santa Cruz</h3>
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3799.737212541813!2d-63.200753582556146!3d-17.7570171!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x93f1e7f495bcd231%3A0x63cc584e6e531078!2sRolea%20Center!5e0!3m2!1ses!2sbo!4v1647619893547!5m2!1ses!2sbo"
                        width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
            </div>
        </div>
    </section>
    {{-- sección contacto --}}
    <section class="contactanos">
        <div class="container">
            @livewire('frm-contacto')
            {{-- <x-frm-contacto></x-frm-contacto> --}}
        </div>
    </section>
    {{-- end diseño estructural --}}
    {{-- sección últimos pryectos --}}

    {{-- end últimos pryectos --}}
    {{-- sección nuestros clientes --}}
    {{-- end nuestros clientes --}}
@endsection
