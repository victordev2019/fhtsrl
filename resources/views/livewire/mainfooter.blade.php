<footer class="main-footer py-10">
    <div class="container">
        <div class="grid grid-cols-1 md:grid-cols-2 gap-6">
            <div class="hidden form-contacto bg-pink-500">
                contactos
            </div>
            <div class="info">
                <h3 class="uppercase mb-2">fahrenheit s.r.l.</h3>
                <p>Nosotros somos Fahrenheit, una empresa especializada en Ingeniería civil y arquitectura que participa
                    activamente en la industria de la construcción, consultorías, capacitación para profesionales y
                    formación académica.
                </p>
                <p class="mt-3">Nuestra trayectoria y crecimiento se encuentra en desarrollo desde el 2012,
                    brindando a nuestros clientes resultados de puntualidad, responsabilidad, seriedad y eficiencia con
                    más de 8 años de experiencia en el mercado.
                </p>
            </div>
            <div class="enlaces-rapidos">
                <div class="content-enlaces">
                    <h3 class="uppercase mb-2">enlaces rápidos</h3>
                    <ul class="uppercase font-medium">
                        <li class=""><a href="/">inicio</a></li>
                        <li class=""><a href="{{ route('geo5') }}">geo5</a></li>
                        <li class=""><a href="{{ route('servicios') }}">servicios</a></li>
                        <li class=""><a href="{{ route('proyectos') }}">proyectos</a></li>
                        <li class=""><a href="{{ route('acercade') }}">acerca de</a></li>
                        <li class=""><a href="{{ route('contactos') }}">contactos</a></li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="adress text-center mt-4">
            <h3>
                LA PAZ
            </h3>
            <p>
                Av. 6 de Agosto. Edificio Alianza N 2190
                Primer Piso Oficinas 105/107/108<br>
                73725190-76725400
            </p>
            <h3 class="mt-2">
                SANTA CRUZ
            </h3>
            <p>
                Equipetrol Norte. Edificio Rolea Center N 120
                Primer Piso Oficina 1A<br>
                70226327-67865550
            </p>
        </div>
        <div class="sociales flex justify-center">
            <div class="social-content text-5xl lg:text-6xl">
                <a href="https://www.facebook.com/FahrenheitIngenieriaEspecializada/" target="_blanck"><i
                        class="fab fa-facebook-square"></i></a>
                <a href="https://www.instagram.com/?hl=es-la" target="_blanck"><i
                        class="fab fa-instagram-square"></i></a>
                <a href="https://www.youtube.com/channel/UCL9MzVw7_AGfKzp4RVLDYMA?view_as=subscriber"
                    target="_blanck"><i class="fab fa-youtube"></i></a>
                <a href="https://wa.me/59176725400?text=Realice cualquier consulta aquí " target="_blanck"><i
                        class="fab fa-whatsapp"></i></a>
            </div>
        </div>
        <hr>
        <div class="copyright flex justify-center">
            <p class="text-center">
                © 2021 <span class="footer-name font-bold">FAHRENHEIT S.R.L.</span> Todos los
                derechos reservados. Desarrollado por
                <span class="reservados font-semibold uppercase">FAHRENHEIT systems</span>
            </p>
        </div>
    </div>
</footer>
