<div>
    <div class="flex items-center min-h-screen bg-gray-50 dark:bg-gray-900">
        <div class="container mx-auto">
            <div class="border-gray-300 border max-w-lg mx-auto my-10 bg-white p-5 rounded-xl shadow-sm">
                <div class="text-center">
                    <h1 class="my-3 text-3xl font-semibold text-gray-700 dark:text-gray-200">Contacta con nosotros</h1>
                    <p class="text-gray-400 dark:text-gray-400">Complete el siguiente formulario para enviarnos un
                        mensaje.</p>
                </div>
                <div class="m-7">
                    <div id="form">
                        <div class="mb-6">
                            <label for="name" class="block mb-2 text-sm text-gray-600 dark:text-gray-400">
                                Nombre</label>
                            <input wire:model.defer="nombre" type="text" name="name" id="name" placeholder="John Doe"
                                required
                                class="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500" />
                            <x-jet-input-error for="nombre"></x-jet-input-error>
                        </div>
                        <div class="mb-6">
                            <label for="email" class="block mb-2 text-sm text-gray-600 dark:text-gray-400">Dirección de
                                correo</label>
                            <input wire:model.defer="email" type="email" name="email" id="email"
                                placeholder="you@company.com" required
                                class="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500" />
                            <x-jet-input-error for="email"></x-jet-input-error>
                        </div>
                        <div class="mb-6">

                            <label for="phone" class="text-sm text-gray-600 dark:text-gray-400">* Teléfono &
                                Móvil</label>
                            <input wire:model.defer="phone" type="text" name="phone" id="phone"
                                placeholder="+1 (555) 77880033"
                                class="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500" />
                        </div>
                        <div class="mb-6">
                            <label for="message"
                                class="block mb-2 text-sm text-gray-600 dark:text-gray-400">Mensaje</label>

                            <textarea wire:model.defer="mensaje" rows="5" name="message" id="message"
                                placeholder="Your Message"
                                class="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500"
                                required></textarea>
                            <x-jet-input-error for="mensaje"></x-jet-input-error>
                        </div>
                        {{-- reCapcha --}}
                        {{-- <div class="recapcha">
                            <button class="g-recaptcha" data-sitekey="6LcF_GUbAAAAAFEq8Z-rjyBNy1i-eYnmmaRGdlne"
                                data-callback='onSubmit' data-action='submit'>Submit</button>
                        </div> --}}
                        <div class="mb-6">
                            <button wire:click="send"
                                class="w-full px-3 py-4 text-white bg-indigo-500 rounded-md focus:bg-indigo-600 focus:outline-none hover:bg-indigo-600">Enviar
                                Mensaje</button>
                        </div>
                        <p class="text-base text-center text-gray-400" id="result">
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- ********** tmp ********* --}}
{{-- form tmp --}}
