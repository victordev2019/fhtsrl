@extends('layouts.main')
@section('title', 'Inicio')
@section('description', 'Ingeniería especializada')
@section('content')
    {{-- main banner --}}
    <div class="main-banner mb-8">
        <div class="container max-w-7xl flex items-center">
            <div class="grid grid-cols-1 md:grid-cols-2 gap-2 z-10">
                {{-- hero left --}}
                <div class="hero-left self-center py-2 md:py-6">
                    <h1 class="wow animate__animated animate__backInDown uppercase font-bold py-1 md:py-2 text-4xl lg:text-5xl"
                        data-wow-duration="1s">
                        fahrenheit s.r.l.</h1>
                    <h3 class="wow animate__animated animate__backInUp uppercase font-normal py-1 text-2xl lg:text-4xl"
                        data-wow-delay="1s" data-wow-duration="2s">
                        ingeniería especializada.</h3>
                    <p class="wow animate__animated animate__fadeIn text-base lg:text-xl pb-2 tracking-wider"
                        data-wow-duration="4s" data-wow-delay="2s">Empresa
                        constructora
                        especializada en Ingeniería
                        civil y arquitectura, que participa activamente en la industria de la construcción, consultorías,
                        distribucion de Software, capacitación para profesionales y formación académica.</p>
                    <div class="link-banner py-4">
                        <a class="wow animate__animated animate__bounceInLeft btn" data-wow-delay="3s" data-wow-duration="2s"
                            href="https://wa.me/59176725400?text=Realice cualquier consulta aquí "
                            target="_blanck">contactar</a>
                        <a class="wow animate__animated animate__bounceInLeft btn-outline ml-4" data-wow-delay="2.5s"
                            data-wow-duration="2s" href="./#tag_servicios">ver
                            más...</a>
                    </div>
                </div>
                {{-- hero right --}}
                <div class="hero-right">
                    <div class="contenedor-cubo">
                        <div class="cara">
                            <div class="item item-1">
                                <h4 class="uppercase z-10 font-normal text-base md:text-xl text-white">construcción</h4>
                            </div>
                            <div class="item item-2">
                                {{-- <h4 class="uppercase">geo 5</h4> --}}
                                <img class="px-2" src="/img/logo_geo5_ bolivia.png" alt="" srcset="">
                            </div>
                            <div class="item item-3">
                                <h4 class="uppercase z-10 font-normal text-base md:text-xl text-white">capacitación</h4>
                            </div>
                            <div class="item item-4">
                                <h4 class="uppercase z-10 font-normal text-base md:text-xl text-white">cursos</h4>
                            </div>
                            <div class="item item-5">
                                <h4 class="uppercase z-10 font-normal text-base md:text-xl text-white">consultoría</h4>
                            </div>
                            <div class="item item-6">
                                {{-- <h4 class="uppercase">fahrenheit</h4> --}}
                                <img class="px-2" src="/img/fht_new_logo_white.svg" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end main banner --}}
    {{-- aside servicios --}}
    <aside id="tag_servicios" class="aside-servicios py-8">
        <div class="container">
            <div class="grid md:grid-cols-2 lg:grid-cols-3 gap-4">
                <a href="{{ route('servicios') . '/#tag_construccion' }}"
                    class="transition duration-500 ease-in-out transform scale-100 hover:scale-105">
                    <div class="wow animate__animated animate__zoomIn item-servicio-n card flex items-center md:h-48"
                        data-wow-duration="1s">
                        <i class="fas fa-hard-hat text-6xl md:text-8xl mr-3 w-20 md:w-auto"></i>
                        <div class="servicio-info flex-1">
                            <h4 class="font-medium">construcción</h4>
                            <p class="hidden md:inline-block">Proyectos enfocados en el desarrollo de construcciones en sus
                                diferentes especialidades.</p>
                        </div>
                        <i class="fas fa-angle-right text-4xl flex-wrap md:hidden"></i>
                    </div>
                </a>
                <a href="{{ url('servicios/#tag_consultoria') }}"
                    class="transition duration-500 ease-in-out transform scale-100 hover:scale-105">
                    <div class="wow animate__animated animate__zoomIn item-servicio card flex items-center md:h-48"
                        data-wow-duration="1s" data-wow-delay=".5s">
                        <i class="fas fa-headset text-6xl md:text-8xl mr-3 w-20 md:w-auto"></i>
                        <div class="servicio-info flex-1">
                            <h4 class="font-medium">Consultoría Especializada</h4>
                            <p class="hidden md:inline-block">De proyectos, Inspeccion Técnica y Administrativa.</p>
                        </div>
                        <i class="fas fa-angle-right text-4xl flex-wrap md:hidden"></i>
                    </div>
                </a>
                <a href="{{ route('geo5') }}"
                    class="transition duration-500 ease-in-out transform scale-100 hover:scale-105">
                    <div class="wow animate__animated animate__zoomIn item-servicio-n card flex items-center md:h-48"
                        data-wow-duration="1s" data-wow-delay="1s">
                        <i class="fas fa-cloud-download-alt text-6xl md:text-8xl mr-3 w-20 md:w-auto"></i>
                        <div class="servicio-info flex-1">
                            <h4 class="font-medium">geo5 software</h4>
                            <p class="hidden md:inline-block">Paquetes y programas individuales.</p>
                        </div>
                        <i class="fas fa-angle-right text-4xl flex-wrap md:hidden"></i>
                    </div>
                </a>
                <a href="{{ url('servicios/#tag_geo') }}"
                    class="transition duration-500 ease-in-out transform scale-100 hover:scale-105">
                    <div class="wow animate__animated animate__zoomIn item-servicio card flex items-center md:h-48 hover:scale-90"
                        data-wow-duration="1s" data-wow-delay="1.5s">
                        <i class="fas fa-cogs text-6xl md:text-8xl mr-3 w-20 md:w-auto"></i>
                        <div class="servicio-info flex-1">
                            <h4 class="font-medium">geo5 soporte</h4>
                            <p class="hidden md:inline-block">Consultoría Geo5, soluciones para proyectos en geotecnia.</p>
                        </div>
                        <i class="fas fa-angle-right text-4xl flex-wrap md:hidden"></i>
                    </div>
                </a>
                <a href="{{ url('servicios/#tag_capacitacion') }}"
                    class="transition duration-500 ease-in-out transform scale-100 hover:scale-105">
                    <div class="wow animate__animated animate__zoomIn item-servicio-n card flex items-center md:h-48"
                        data-wow-duration="1s" data-wow-delay="2s">
                        <i class="fas fa-book text-6xl md:text-8xl mr-3 w-20 md:w-auto"></i>
                        <div class="servicio-info flex-1">
                            <h4 class="font-medium">capacitación profesional</h4>
                            <p class="hidden md:inline-block">Programas y cursos de formación especializados en ingeniería
                                civil.</p>
                        </div>
                        <i class="fas fa-angle-right text-4xl flex-wrap md:hidden"></i>
                    </div>
                </a>
                <a href="{{ url('servicios/#tag_asesoria') }}"
                    class="transition duration-500 ease-in-out transform scale-100 hover:scale-105">
                    <div class="wow animate__animated animate__zoomIn item-servicio card flex items-center md:h-48"
                        data-wow-duration="1s" data-wow-delay="2.5s" data-wow-offset="10">
                        <i class="fas fa-graduation-cap text-6xl md:text-8xl mr-3 w-20 md:w-auto"></i>
                        <div class="servicio-info flex-1">
                            <h4 class="font-medium">asesoría académica</h4>
                            <p class="hidden md:inline-block">Asesoría integral en tesis y proyectos de grado en ingeniería
                                civil.</p>
                        </div>
                        <i class="fas fa-angle-right text-4xl flex-wrap md:hidden"></i>
                    </div>
                </a>
            </div>
        </div>
    </aside>
    {{-- end aside servicios --}}
    {{-- sección últimos pryectos --}}
    <section id="tag_proyectos" class="ultimos-proyectos py-8 my-8 z-0">
        <div class="container relative z-10">
            <h2 class="uppercase mb-8 font-normal">últimos proyectos</h2>
            <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-5 mb-20">
                <a href="{{ route('proyectos') }}">
                    <div class="item">
                        <img src="/img/proyectos/proyecto5.jpg" alt="" srcset="">
                        <div class="legend p-3">
                            <h4 class="uppercase">construcción y refuerzo estructural</h4>
                            {{-- <p>Pantalla led mercado Lanza.</p> --}}
                        </div>
                    </div>
                </a>
                <a href="{{ route('proyectos') }}">
                    <div class="item">
                        <img src="/img/proyectos/proyecto3.jpg" alt="" srcset="">
                        <div class="legend p-3">
                            <h4 class="uppercase">Diseño de fundaciones para máquinas</h4>
                            {{-- <p>Lorem ipsum adipisicing elit.</p> --}}
                        </div>
                    </div>
                </a>
                <a href="{{ route('proyectos') }}" class="hidden lg:inline-block">
                    <div class="item">
                        <img src="/img/proyectos/proyecto4.jpg" alt="" srcset="">
                        <div class="legend p-3">
                            <h4 class="uppercase">Diseño de fundaciones para máquinas</h4>
                            {{-- <p>Lorem ipsum adipisicing elit.</p> --}}
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>
    {{-- end últimos pryectos --}}
    {{-- sección diseño estructural --}}
    <section id="tag_diseno" class="diseno mt-12 py-8 my-8 z-0">
        <div class="container relative z-10">
            <h2 class="uppercase text-white">cálculo & diseño</h2>
            <div class="item-wrap grid grid-cols-1 md:grid-cols-3 gap-4">
                <a href="{{ url('servicios/#tag_diseno') }}" class="item wow animate__animated animate__flip">
                    <div class="p-8">
                        <h3 class="uppercase">cálculo</h3>
                        <i class="fas fa-square-root-alt text-5xl p-8"></i>
                        <p class="">Cálculos para obtener el diseño más eficiente.</p>
                    </div>
                </a>
                <a href="{{ url('servicios/#tag_diseno') }}" class="item wow animate__animated animate__flip"
                    data-wow-delay=".5s">
                    <div class="p-8">
                        <h3 class="uppercase">diseño</h3>
                        <i class="fas fa-drafting-compass text-5xl p-8"></i>
                        <p>Especialistas en diseño y construcción.</p>
                    </div>
                </a>
                <a href="{{ url('servicios/#tag_arquitectura') }}" class="item wow animate__animated animate__flip"
                    data-wow-delay="1s">
                    <div class="p-8">
                        <h3 class="uppercase">arquitectura</h3>
                        <i class="fas fa-building text-5xl p-8"></i>
                        <p class="">Tendencias, Diseños actualizados y personalizados.</p>
                    </div>
                </a>
            </div>
        </div>
    </section>
    {{-- end diseño estructural --}}
    {{-- sección nuestros clientes --}}
    <section class="nuestros-clientes py-8">
        <div class="container">
            <h2 class="uppercase pb-6 font-medium">nuestros clientes</h2>
            <div class="cliente-logo flex flex-wrap justify-center">
                <img src="/img/clientes/02.jpg" alt="" srcset="" class="m-4 lg:m-8">
                <img src="/img/clientes/03.jpg" alt="" srcset="" class="m-4 lg:m-8">
                <img src="/img/clientes/04.png" alt="" srcset="" class="m-4 lg:m-8">
                <img src="/img/clientes/05.jpg" alt="" srcset="" class="m-4 lg:m-8">
                <img src="/img/clientes/06.jpg" alt="" srcset="" class="m-4 lg:m-8">
            </div>
        </div>
    </section>
    {{-- end nuestros clientes --}}
    {{-- seccion formulario de contactos --}}
    {{-- sección contacto --}}
    <section class="contactanos">
        <div class="container">
            @livewire('frm-contacto')
            {{-- <x-frm-contacto></x-frm-contacto> --}}
        </div>
    </section>
    {{-- end diseño estructural --}}
@endsection
