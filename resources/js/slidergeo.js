const imagenes = new Array(
    "/img/banner1.jpg",
    "/img/banner2.jpg",
    "/img/banner3.jpg"
);
let index = 0;
// console.log(imagenes);
const slides = document.querySelector(".slider").children;
console.log(slides);

// chageSlide();

const indicators = document.querySelector(".indicators");
circleIndicators();

const prev = document.getElementById("prev");
prev.addEventListener("click", prevSlide);

const next = document.getElementById("next");
next.addEventListener("click", nextSlide);

function circleIndicators() {
    const point = new Array();
    for (let i = 0; i < slides.length; i++) {
        const div = document.createElement("div");
        // div.innerHTML = i + 1;
        // div.addEventListener("click", indicateSlide(this));
        div.setAttribute("id", i);
        indicators.appendChild(div);
        point[i] = document.getElementById(i);
        point[i].addEventListener("click", function () {
            index = i;
            console.log(index);
            chageSlide();
            updateCircleIndicator();
        });
        if (i == 0) {
            console.log(point);
            div.className = "point";
        }
    }
}

function updateCircleIndicator() {
    for (let i = 0; i < indicators.children.length; i++) {
        indicators.children[i].classList.remove("point");
    }
    indicators.children[index].classList.add("point");
}

function indicateSlide() {
    console.log("clic ok");
    index = 1;
    chageSlide();
}

function prevSlide() {
    if (index == 0) {
        index = slides.length - 1;
    } else {
        index--;
    }
    chageSlide();
    updateCircleIndicator();
    // console.log(imagenes.length);
}

function nextSlide() {
    if (index == slides.length - 1) {
        index = 0;
        // console.log(index);
    } else {
        index++;
    }
    chageSlide();
    updateCircleIndicator();
    // console.log(imagenes.length);
}

function chageSlide() {
    console.log(index);
    for (let i = 0; i < slides.length; i++) {
        slides[i].classList.remove("slide-active");
    }
    slides[index].classList.add("slide-active");
    // img.style.backgroundImage = "url(" + imagenes[index] + ")";
}

function autoplay() {
    nextSlide();
}
let timer = setInterval(autoplay, 5000);
// console.log(izq);
