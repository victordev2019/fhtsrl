const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    purge: [
        "./vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php",
        "./vendor/laravel/jetstream/**/*.blade.php",
        "./storage/framework/views/*.php",
        "./resources/views/**/*.blade.php",
    ],

    theme: {
        container: {
            center: true,
        },
        backgroundImage: {
            "hero-pattern": "url('/img/banner1.jpg')",
            "footer-texture": "url('/img/banner4.jpg')",
            "banner-direcction": "url('/img/direccion.png')",
            "banner-servicios": "url('/img/banner_servicios.jpg')",
            "banner-proyectos": "url('/img/banner_proyectos.jpg')",
        },
        extend: {
            fontFamily: {
                sans: ["Nunito", ...defaultTheme.fontFamily.sans],
            },
        },
    },

    variants: {
        extend: {
            opacity: ["disabled"],
        },
    },

    plugins: [
        require("@tailwindcss/forms"),
        require("@tailwindcss/typography"),
    ],
};
