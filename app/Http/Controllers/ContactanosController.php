<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactanosMailable;
use Illuminate\Support\Facades\Mail;

class ContactanosController extends Controller
{
    public function index()
    {
        # code...
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'mensaje' => 'required'
        ]);
        $correo = new ContactanosMailable($request->all());
        Mail::to('admin@fhtsrl.com')->send($correo);
        // return redirect()->route('home');
        return redirect()->to(route("home") . "#contactanos")->with('info', 'Mensaje enviado.');
    }
}
