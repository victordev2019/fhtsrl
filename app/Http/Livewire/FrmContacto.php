<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Mail\ContactanosMailable;
use Illuminate\Support\Facades\Mail;

class FrmContacto extends Component
{
    public $nombre, $email, $phone, $mensaje;
    protected $rules = [
        'nombre' => 'required|max:25',
        'email' => 'required|email',
        'mensaje' => 'required',
    ];
    public function render()
    {
        // $this->nombre = 'Victor';
        return view('livewire.frm-contacto');
    }
    public function send()
    {
        $this->validate();
        $contacto = [
            'name' => $this->nombre,
            'email' => $this->email,
            'phone' => $this->phone,
            'mensaje' => $this->mensaje
        ];
        $correo = new ContactanosMailable($contacto);
        Mail::to('gerencia@fhtsrl.com')->send($correo);
        // Mail::to('tigervat10@gmail.com')->send($correo);
        // dd($contacto);
        $this->reset(['nombre', 'email', 'phone', 'mensaje']);
        $this->emit('alert', 'Mensaje enviado!', 'Gracias por enviarnos su mensaje, nosotros nos pondremos en contacto para responder a su inquietud.');
    }
}
