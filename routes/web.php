<?php

use App\Http\Controllers\ContactanosController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactosController;
use App\Http\Controllers\GeoController;
use App\Http\Controllers\NosotrosController;
use App\Http\Controllers\ProyectosController;
use App\Http\Controllers\ServiciosController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
// rutas vat code
Route::get('contactos', ContactosController::class)->name('contactos');
Route::get('acercade', NosotrosController::class)->name('acercade');
Route::get('proyectos', ProyectosController::class)->name('proyectos');
Route::get('servicios', ServiciosController::class)->name('servicios');
Route::get('geo5', GeoController::class)->name('geo5');
// Mail
Route::get('contactanos', function () {
});
Route::post('contactanos', [ContactanosController::class, 'store'])->name('contactanos.store');
